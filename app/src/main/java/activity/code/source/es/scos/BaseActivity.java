package activity.code.source.es.scos;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(this.getClass().getSimpleName(), "onCreate(): 创建");
        ActivityUtils.addActivity(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        Log.d(this.getClass().getSimpleName(), "-> onStart(): 开始");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.d(this.getClass().getSimpleName(), "-> onResume: 预备");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.d(this.getClass().getSimpleName(), "-> onPause(): 暂停");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.d(this.getClass().getSimpleName(), "-> onStop: 停止");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        ActivityUtils.removeActivity(this);
        Log.d(this.getClass().getSimpleName(), "-> onDestroy(): 销毁");
        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        Log.d(this.getClass().getSimpleName(), "-> onRestart(): 复活");
        super.onRestart();
    }


}
