package activity.code.source.es.scos;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import es.source.code.model.Food;


/**
 * A simple {@link Fragment} subclass.
 */
public class DrinkFoodFragment extends BaseFoodFragment {

    @Override
    public void initFoods() {
        for (int i = 0;i < 3;i++){
            Food coldFood = new Food(R.drawable.ic_wine,"10","啤酒");
            foodlist.add(coldFood);
            Food coldFood_2 = new Food(R.drawable.ic_wine,"20","红酒");
            foodlist.add(coldFood_2);
            Food coldFood_3 = new Food(R.drawable.ic_wine,"30","茅台");
            foodlist.add(coldFood_3);
        }
    }
}
