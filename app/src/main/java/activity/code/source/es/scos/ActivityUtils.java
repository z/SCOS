package activity.code.source.es.scos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ActivityUtils {
//     无限扩容的数组
    public static final List<Activity> allActivity = new ArrayList<>();
    public  static void addActivity(Activity activity){
        allActivity.add(activity);
    }

    public  static void removeActivity(Activity activity){
        allActivity.remove(activity);
    }

    public  static void finishAll(){
        Iterator<Activity> iterator = allActivity.iterator();
        while(iterator.hasNext()){
            Activity each = iterator.next();
            if(!each.isFinishing()){
                each.finish();
            }
            iterator.remove();
        }
    }

//     专门做 活动之间的跳转
    public static void actionStart(Context context,Class clazz, String... param) {
        Intent intent = new Intent(context, clazz);
        if(param!=null && param.length>0){
            for (int i = 0; i <param.length ; i++) {
                intent.putExtra("param"+i, param[i]);
            }
        }
        context.startActivity(intent);
    }
// 显示一个长时 Toast
    public static void showLongToast(Context context,String text){
        Toast.makeText(context,text,Toast.LENGTH_LONG).show();
    }
    // 显示一个短时 Toast
    public static void showShortToast(Context context,String text){
        Toast.makeText(context,text,Toast.LENGTH_SHORT).show();
    }


}
